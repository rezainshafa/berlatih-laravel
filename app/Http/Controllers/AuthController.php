<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }


    public function kirim(Request $req) {
        $fname = $req->first;
        $lname = $req->last;
        return view('welcome', compact('fname','lname'));
    }

    public function login() {
        return view('login');
    }

    public function masuk() {
        return view('kembali');
    }
}
