<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>WELCOME {{$fname}}</title>
</head>
<body>
    <div class="mx-5 my-5">
    <h1>SELAMAT DATANG {{$fname}} {{$lname}} !</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media Kita Bersama!</h2>
    </div>
</body>
</html>