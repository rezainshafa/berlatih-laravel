<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Form Sign Up</title>
</head>

<body>
    <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link" href="/home">HOME</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/register">REGISTER</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/login">LOGIN</a>
      </ul><br>

    <div class="mx-4 my-2">
    <h1 style="align-content: center">Buat Account Baru!</h1>
    <h3>Sign Up Form</h3><br>
    <form action="/welcome" method="POST">
        @csrf
    <table>
        <tr>
            <td>First Name</td>
            <td><input required type="text" name="first" ></td>
        </tr>

        <tr>
            <td>Last Name</td>
            <td><input required type="text" name="last" ></td>
        </tr>

        <tr>
            <td>Gender</td>
            <td><input type="radio" name="kelamin" >Male
                <input type="radio" name="kelamin" >Female
                <input required type="radio" name="kelamin" >Other
            </td>
        </tr>

        <tr>
            <td>Nationality</td>
            <td><select>
                    <option value="Indonesian">Indonesian</option>
                    <option value="Singapore">Singapore</option>
                    <option value="Indonesia">Indonesia</option>
                    <option value="Malaysia">Malaysia</option>
                    <option value="Australia">Australia</option>
                </select>
            </td>
        </tr>

        <tr>
            <td>Language Spoken</td>
            <td><input required type="checkbox" > Indonesia 
                <input type="checkbox"> English 
                <input type="checkbox"> Other
            </td>
        </tr>

        <tr>
            <td>Bio</td>
            <td><textarea name="bio" cols="30" rows="10"></textarea></td>
        </tr>

        <tr>
            <td></td>
            <td><input type="submit" value="Sign Up"></td>
        </tr>
    </table>
    </form>
</div>
</body>

</html>