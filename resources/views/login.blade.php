<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Login Page</title>
</head>
<body>
    <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
          <a class="nav-link" href="/home">HOME</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/register">REGISTER</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/login">LOGIN</a>
      </ul><br>

    <div class="container-fluid">
        <div class="row my-5">
            <div class="col-3"></div>
            <div class="col-6"><div class="container mt-4 justify-content-center">
                <h5 style="text-align: center; font-family: monospace;">
                    ALREADY A MEMBER SANBERBOOK?</h5><br>
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Addres</label>
                        <input required type="email" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                            else.</small>
                    </div>

                    <form action="/kembali" method="POST">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input required type="password" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Send me occasional product
                            updates.</label>
                    </div>
                    <input type="submit" value="Login">
                </form>
            </div></div>
            <div class="col-3"></div>
        </div>
    </div>
</body>
</html>